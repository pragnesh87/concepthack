<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use App\Imports\ExamQuestionsImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FileImportTest extends TestCase
{
    use RefreshDatabase;
    public function test_can_import_file()
    {
        //$this->withExceptionHandling();
        $file = new UploadedFile(
            public_path('tests/mcq_import.xlsx'),
            'mcq_import.xlsx',
            null,
            null,
            true
        );

        //Excel::fake();

        $this->post(route('import.save'), [
            'mcq_file' => $file
        ])
            ->assertSessionHas('success', 'MCQ imported successfully')
            ->assertRedirect('/import');

        $data = Excel::toArray(new ExamQuestionsImport, $file);
        $record = $data[0][0];

        $this->assertDatabaseHas('exams', ['name' => $record['username']]);
        $this->assertDatabaseHas('questions', ['question' => $record['question']]);
    }

    public function test_excel_file_type_required()
    {
        $file = new UploadedFile(
            public_path('tests/test.txt'),
            'test.txt',
            null,
            null,
            true
        );

        $this->post(route('import.save'), [
            'mcq_file' => $file
        ])->assertSessionHasErrors('mcq_file');
    }
}