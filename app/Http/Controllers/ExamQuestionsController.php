<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\ExamQuestionsImport;
use Maatwebsite\Excel\Facades\Excel;

class ExamQuestionsController extends Controller
{
    public function index()
    {
        return view('exam.import');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'mcq_file' => ['required', 'mimes:xls,xlsx']
        ]);

        $filePath = $request->file('mcq_file')
            ->getRealPath();

        Excel::import(new ExamQuestionsImport, $filePath);

        return redirect('/import')->with('success', 'MCQ imported successfully');
    }
}