<?php

namespace App\Imports;

use App\Models\Exam;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ExamQuestionsImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        foreach ($collection as $row) {
            $exam = Exam::create(['name' => $row['username']]);
            $exam->questions()->create([
                'question' => $row['question'],
                'option1' => $row['option1'],
                'option2' => $row['option2'],
                'option3' => $row['option3'],
                'option4' => $row['option4'],
                'answer' => $row['answer']
            ]);
        }
    }
}