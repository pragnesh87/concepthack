@extends('layouts.app')

@section('title', 'Import File');

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                Import
            </div>
            <div class="card-body">
                @if (session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <form action="{{ route('import.save') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="formFile"
                            class="form-label @error('mcq_file') is-invalid @enderror">@lang('File')</label>
                        <input class="form-control" type="file" name="mcq_file" id="mcq_file" accept=".xlsx, .xls">
                        @error('mcq_file')
                            <div class="invalid-feedback">
                                Please select file
                            </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">@lang('Upload')</button>
                </form>
            </div>
        </div>
    </div>
@endsection
